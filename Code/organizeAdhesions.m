%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Micah Thornton 
%	 A short script to set the options for and read in 
%	 Cell Wall, Pseudo Nucleus, and Adhesion Detection masks
%	 and produce a generative model using the Murphy's Lab 
%	 cellOrganizer Package.
%	
%	 Date: June 5 2018. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%Sets paths to directories with 3D images. DD Cellorganizer
addpath(genpath('/home/micah/UTSW/tools/cellorganizer-v2.7.1'));

%% ADD Bioformats to Java
javaaddpath('/home/micah/UTSW/tools/bfmatlab/bioformats_package.jar');

nucDir = 'NuclearMembrane'; 
plaDir = 'PlasmaMembrane'; 
adhDir = 'Adhesions'; 

nucPaths = [nucDir '/fakeNucleusMask-movie-*.tif'];
plaPaths = [plaDir '/cellMask-movie-*.tif'];
adhPaths = [adhDir '/detectionMask-movie-*.tif'];


%Sets up the options structure for CellOrganizer
opt.model.resolution = [0.104, 0.104, 0.200];
opt.train.flag = 'all'; 
opt.model.filename = 'AdhesionModel.mat'; 
opt.nucleus.type = 'cylindrical_surface'; 
opt.cell.type = 'ratio'; 
opt.protein.type = 'gmm'; 
opt.protein.class = 'vesicle'; 
opt.nucleus.class = 'nuclear_membrane';
opt.cell.class = 'cell_membrane'; 
opt.downsampling = [5,5,1]; 
opt.debug = true; 
opt.segminnucfraction = 0;

%Train the model 
img2slml('3D', nucPaths, plaPaths, adhPaths, opt); 

modPath = {'model.mat'}; 
%Sets Image Production Options
op.targetDirectory = './SyntheticAdhesions/';
op.prefix = 'adhesion';
op.numberOfSynthesizedImages = 10; 


%%Produce Some images from the created model
rng(147)
slml2img(modPath,op);

%%The following is a vizualization of the produced cell images
imCell = ml_readimage('SyntheticAdhesions/adhesion/cell1/cell.tif');
imDNA = ml_readimage('SyntheticAdhesions/adhesion/cell1/nucleus.tif');
imPro = ml_readimage('SyntheticAdhesions/adhesion/cell1/vesicle1.tif');


imInd = zeros(size(imCell));

imInd(imCell>0)=1;
imInd(imDNA>0)=2;
imInd(imPro>0)=3;

figure, img2vol(ml_downsize(imInd,[2,2,2]));

